FROM node:8.11.0-alpine

# Install newer npm version
ENV NPM_VERSION 5.8.0
RUN npm i -g npm@${NPM_VERSION}

WORKDIR /home/app