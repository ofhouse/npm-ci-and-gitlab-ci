// Simulates a test / build process

require('yargs')
  .command(
    'build [platform]',
    'runs build script',
    yargs => {
      yargs.positional('platform', {
        describe: 'target platform',
        default: 'win',
      });
    },
    argv => {
      console.log(`Build for platform ${argv.platform} successful!`);
    }
  )
  .command('test', 'runs tests', () => {
    console.log(`All tests completed!`);
  }).argv;
